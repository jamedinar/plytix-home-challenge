## NOTES ABOUT THE CHALLENGE

### ASSUMPTIONS
All assumptions have to be checked with the client before spending time working on them. For this reason, in case of doubt, I have implemented the easy solution.

  * "Word" field can only contain letters in the spanish language (of course ascii, english, etc are included in that superset). To reduce the possibility of noSQL injection or XSS we sanitize the user input, allowing only spanish set of characters. The allowed charset can be extended in case it's required. If a not allowed character is found in the word, it will be discarded and a 400 error code will be sent.
  * For the "Position" field only integer numbers are allowed (strings with digit chars are not valid). Invalid "Position" will generate response with 400 error.
  * Gaps are not allowed in the list. Words are always inserted in the position min(position, number of words + 1). Deleting a word will reduce the position for all words behind.
  * Word will be UNIQUE in the database. No duplicates allowed. For this reason, autoindex (object "_id"), would not be needed in the database, allowing us to save some memory. Unfortunately, mongoDB atlas is not allowing to create a collection with autoIndexId set to false. In principle, position could also be unique, but it will give errors when trying to increment the positions for several words atomically with pymongo.update_many().
  * POSTing an already existing word will do nothing and return error code 409 with informative message. A different handling could be also done easily (update the position, allow word duplicates...).
  * PATCH / DELETE a non-existing word will return 404 error code.
  * All communications are using JSON data.


### ANAGRAMS
  * Easiest way to find if two words are anagrams is sorting by characters and checking for equality.
  * We consider upper/lower case letters to be the same, so for the comparison we set all as lowercase.
  * Characters with/without accent mark are considered different
  * Clarification is needed about if upper/lower case words are different and if the variations should be stored on the DB. With the current implementation you could get a list with duplicated words that differ on the letters case.


### ABOUT THE DB
  * I created a free mongoDB atlas account to avoid installing mongoDB on my machine, almost everything is in the cloud now anyway :P
  * I am allowing everyone access by whitelisting the ip access (0.0.0.0/0), so you should be able to use it when testing the app.
  * There are two separated databases:
    * Production: To be used in the deployed tool.
    * Test: Used for tests, it is completely reset (all elements deleted) before tests are run.
  * The name of the collection used by the app is "words".


### ABOUT THE IMPLEMENTATION
  * We can use **Flask-RESTful** or other extensions like **Flask-RESTx** (for swagger API documentation generation). Since this is a simple exercise we use vanilla Flask for the API.
  * I used low level **pymongo** to communicate with the DB. But later I found the existence of **MongoEngine**. I think this **MongoEngine** is what I would use next time, it is higher level and closer to Django style. I think it is much better, specially when managing different data models.
  * Storing the word list in the database could be done in different ways:
    * Saving an ordered list in a single document could be an option if we are not planning on storing a huge list (enough for this exercise :P).
        * Advantages: Easier to implement, less memory consumption (no need to save the position number).
        * Drawbacks: MongoDB document limit is 16MB. DB search/filter capabilities are not used since we are always retrieving the full list.
    * Saving word/position pair in each document. **This is the chosen solution**. It provides better use of the DB capabilities and would be easier to scale when going big.
  * I did everything in windows, so the linux scripts are not really tested (just in case you use them). I hope they work though :P


### PYTHON VIRTUAL ENVIRONMENT
  * Create python virtual environment with requirements.txt:
    * *virtualenv venv --python=python3*
    * Activate the virtual environment:
        * **LINUX:** *source venv/bin/activate*
        * **WINDOWS:** *.\venv\Scripts\activate*
    * *pip install -r requirements.txt*


### TESTING / PRODUCTION ENVIRONMENT
  * SECURITY ISSUE: never save the passwords in the app code or the repository!!
  * We have two environment files for each OS (windows, linux) in the repo (bad practice). They are used to load the database connection string for production or tests.
  * Usually, these variables should be set when creating the virtual environment or docker, but not stay in the repo / code.


### TESTING
  * During development I used "Postman" for testing manually.
  * Testing uses a separated collection in the database (test). The connection url is taken from an environment variable. Ensure to load the testing environment before running the tests!!
  * Setting testing environment:
    * **LINUX:** *source ./test.env* (linux)
    * **WINDOWS POWERSHELL:** *.\test.windows.env.ps1*
  * Run tests:
    * *pytest*
  * The 4 functional tests should run green :)
    * test_adding_words
    * test_patch
    * test_anagrams
    * test_delete
  * Tests are based on the example provided in the mail. For a real product they should be extended to cover corner cases, different anagrams, etc.


### PRODUCTION
  * Setting development environment:
    * **LINUX:** *source ./production.env*
    * **WINDOWS POWERSHELL:** *.\production.windows.env.ps1*
  * To run using flask develoment server (not valid for deployment):
    * *python app.py*

