import os
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo, pymongo
import string

app = Flask(__name__)

# To auto reload the server on changes during development
# TURN OFF ON PRODUCTION DEPLOYMENT!!
app.debug = (os.environ.get("DEBUG") == "TRUE")

# DISABLE ASCII ONLY JSON:
# Only if we want to see a "pretty" output, without /uXXX unicode for non-ascii chars (as "á, ñ..")
# If the API is intended to be consumed by another service that correctly interprets the unicode chars
# this could be removed.
# WARNING: disabling ASCII-safe encoding opens the door for issues with U+2028 and U+2029 separators in the data.
# This could leave you open to a cross-site scripting issue, depending on how the error is handled.
# To avoid this we will validate all data introduced in the DB, allowing only latin characters for the words.
app.config['JSON_AS_ASCII'] = False
WORD_ALLOWED_CHARS = set(string.ascii_letters + string.digits + "ñÑáéíóúÁÉÍÓÚ")

# KEEP THIS IF WE REALLY WANT THE DICT ORDER TO NOT BE ALTERED IN THE JSON (word/position dict in some responses)
# Dict order is not "guaranteed" in python, however in CPython 3.6+ there is insertion order by implementation.
# By default Flask will serialize JSON objects in a way that the keys are ordered.
# This is done in order to ensure that independent of the hash seed of the dictionary
# the return value will be consistent to not trash external HTTP caches.
# This is not recommended but might give you a performance improvement on the cost of cacheability.
# ----> We are changing the default value here by setting it to False <----
# In the implementation we are using there is insertion order so no problems with cacheability :)
# This should be checked in case we use another implementation (PyPy, Jython, etc) or a version
# where dicts do not keep the insertion order!!!
app.config['JSON_SORT_KEYS'] = False

try:
    app.config["MONGO_URI"] = os.environ.get("DATABASE_CONNECT_URL")
    mongo = PyMongo(app)
except Exception as e:
    print('Failed to connect to mongo database server: ' + str(e))
    print("Check that the environment file has been set properly!!")

# "word" could be treated as our index in the database, so we could remove the default "_id" in mongoDB
# Unfortunately this is not possible in the atlas mongodb we are using:
#   - Creating a collection with autoIndexId set to false is disallowed in this atlas tier :(
# wordsDB = pymongo.collection.Collection(mongodb, 'words', autoIndexId=False)

# We assume word and position are unique
wordsDB = mongo.db.words
wordsDB.create_index("word", unique=True)


# Left here as a reference, not really needed...
@app.route('/')
def hello():
    api_description = """API DESCRIPTION:
                         <ul>
                           <li>GET /words/ -> Get the list of words in the database.</li>
                           <li>POST /words/ -> Add a new word in the database. Word and Position should be indicated in the payload.</li>
                           <li>PATCH /words/&ltword&gt -> Modify the position for a word in the database. The new position should be indicated in the payload.</li>
                           <li>DELETE /words/&ltword&gt -> Delete the word indicated from the database.</li>
                           <li>GET /words/&ltword&gt/anagrams -> Get all words in the database that are anagrams of the word provided in the url.</li>
                         </ul>"""
    return api_description


@app.route('/words', methods=['GET'])
def get_words():
    """Get the list of words in the database"""
    data = [word['word'] for word in wordsDB.find().sort("position", pymongo.ASCENDING)]
    # jsonify automatically sets the content type to application/json, thanks Flask :)
    return jsonify({'data': data}), 200


@app.route('/words', methods=['POST'])
def add_word():
    """Add a new word in the database. Word and Position should be indicated in the payload"""
    request_data = request.get_json()
    word = request_data['word']
    # No gaps allowed in position, maximum insertion position is the number of words + 1
    # + 1 because we add a new word to the list!!
    position = min(request_data['position'], wordsDB.count_documents({})+1)

    # Validate the user input
    # Empty words not allowed
    if not len(word):
        return f"Empty word not allowed", 400

    # Only a subset of chars for "words" are allowed
    # This is done to reduce the possibility of noSQL injections and XSS attacks
    # Code to clean the word if desired (line below). Deactivated...
    # word = ''.join([char for char in word if char in WORD_ALLOWED_CHARS])
    for char in word:
        if char not in WORD_ALLOWED_CHARS:
            return f"Not valid word \"{word}\". Only ascii or spanish characters are allowed", 400

    # Validate user input, position must be an integer
    if not isinstance(position, int):
        return f"Not valid position \"{position}\", it must be an integer", 400

    if wordsDB.find_one({"word": word}):
        # Word is already in the database
        # ASSUMPTION -> Do nothing and return error code.
        # Another possibilities could be to update the position, allow duplicates...
        return f"Already existing word \"{word}\", use PATCH if you want to change its position", 409

    # If we are here this a new word
    data = {"word": word, "position": position}
    # We need to make space for the new word
    # All words with position >= new_word_position will increment their position by 1
    wordsDB.update_many({"position": {"$gte": position}}, {'$inc': {'position': 1}})
    # Now we can insert the new word
    wordsDB.insert_one(data)

    # MongoDB "_id" index is added to data by insert_one(), we should remove it before calling jsonify
    data.pop("_id")
    return jsonify(data), 201


@app.route('/words/<string:word>', methods=['PATCH'])
def patch_word(word):
    """Modify the position for a word in the database. The new position should be indicated in the payload"""
    request_data = request.get_json()
    new_position = request_data['position']

    # Validate user input, position must be an integer
    if not isinstance(new_position, int):
        return f"Not valid position \"{new_position}\", it must be an integer", 400

    word_in_db = wordsDB.find_one({"word": word})
    if word_in_db is None:
        return f"The word \"{word}\" does not exists in the database", 404
    old_position = word_in_db["position"]

    # No gaps allowed in position, maximum insertion position is the number of words
    new_position = min(new_position, wordsDB.count_documents({}))

    # Make space for the new word
    if new_position > old_position:
        # All words between (old_position, new_position] will decrement their position by 1
        wordsDB.update_many({"position": {"$gt": old_position, "$lte": new_position}}, {'$inc': {'position': -1}})
    elif new_position < old_position:
        # All words between [new_position, old_position)  will increment their position by 1
        wordsDB.update_many({"position": {"$gte": new_position, "$lt": old_position}}, {'$inc': {'position': 1}})

    # Now we can insert the word in its new position (if it is really new...)
    data = {"word": word, "position": new_position}
    if new_position != old_position:
        wordsDB.replace_one({"word": word}, data)

    return jsonify(data), 200


@app.route('/words/<string:word>', methods=['DELETE'])
def delete_word(word):
    """Delete the word in the url from the database"""
    word_in_db = wordsDB.find_one({"word": word})
    if word_in_db is None:
        return f"The word \"{word}\" does not exists in the database", 404
    position = word_in_db["position"]

    # All words with position > new_word_position will decrement their position by 1
    wordsDB.update_many({"position": {"$gt": position}}, {'$inc': {'position': -1}})

    # Delete the word from the DB
    wordsDB.delete_one({"word": word})
    return "", 204


@app.route('/words/<string:word>/anagrams', methods=['GET'])
def get_word_anagrams(word):
    """Get all words in the database that are anagrams of the word provided in the url"""
    # Easiest way to find if two words are anagrams is sorting the characters and checking for equality
    # We consider upper/lower case letters to be the same, so first we set all as lowercase.
    # With this implementation letters with/without accent mark are considered different.
    def is_anagram(word1, word2):
        return sorted(word1.lower()) == sorted(word2.lower())
    anagrams = [word_db['word'] for word_db in wordsDB.find() if (is_anagram(word_db['word'], word))]
    return jsonify({'data': anagrams}), 200


if __name__ == '__main__':
    app.run()
