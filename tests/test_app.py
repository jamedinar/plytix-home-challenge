import json

INITIAL_TEST_WORDS = ["cosa", "caso", "paco", "pepe", "Málaga"]


def test_adding_words(client):
    # Starting test DB should be empty
    response = client.get('/words')
    assert response.status_code == 200
    expected = {'data': []}
    assert expected == json.loads(response.get_data(as_text=True))

    # Create initial list of words in database
    for index, word in enumerate(INITIAL_TEST_WORDS):
        position = index + 1
        data = {'word': word, 'position': position}
        response = client.post('/words', json=data)
        assert response.status_code == 201
        assert response.content_type == 'application/json'
        assert response.json['word'] == word
        assert response.json['position'] == position

    # Trying to create a duplicated word should fail
    data = {'word': INITIAL_TEST_WORDS[0], 'position': 77}
    response = client.post('/words', json=data)
    assert response.status_code == 409

    # -----------------
    # 1 - GET /words
    # -----------------
    response = client.get('/words')
    assert response.status_code == 200
    expected = {'data': ['cosa', 'caso', 'paco', 'pepe', 'Málaga']}
    assert expected == json.loads(response.get_data(as_text=True))

    # ---------------------------------------------------
    # 2 - POST /words {"word": "calle", "position": 3}
    # ---------------------------------------------------
    # Insert "calle" in position 3
    word = "calle"
    position = 3
    data = {'word': word, 'position': position}
    response = client.post('/words', json=data)
    assert response.status_code == 201
    assert response.content_type == 'application/json'
    assert response.json['word'] == word
    assert response.json['position'] == position

    # ------------------
    #  3 - GET / words
    # ------------------
    # Get the list of words again, calle should be in position 3
    response = client.get('/words')
    assert response.status_code == 200
    expected = {'data': ['cosa', 'caso', 'calle', 'paco', 'pepe', 'Málaga']}
    assert expected == json.loads(response.get_data(as_text=True))


def test_patch(client):
    # -----------------------------------------
    #  4 - PATCH /words/calle {"position": 5}
    # -----------------------------------------
    new_position = 5
    data = {'position': new_position}
    response = client.patch('/words/calle', json=data)
    assert response.status_code == 200
    expected = {"word": "calle", "position": new_position}
    assert expected == json.loads(response.get_data(as_text=True))

    # ----------------
    # 5 - GET /words
    # ----------------
    # "calle" should be in position 5 now...
    response = client.get('/words')
    assert response.status_code == 200
    expected = {'data': ['cosa', 'caso', 'paco', 'pepe', 'calle', 'Málaga']}
    assert expected == json.loads(response.get_data(as_text=True))


def test_anagrams(client):
    # -----------------------------
    # 6- GET /words/asco/anagrams
    # -----------------------------
    response = client.get('/words/asco/anagrams')
    assert response.status_code == 200
    expected = {'data': ['cosa', 'caso']}
    assert expected == json.loads(response.get_data(as_text=True))


def test_delete(client):
    # Delete non existing word
    response = client.delete('/words/asco')
    assert response.status_code == 404

    # ---------------------------
    # 7 - DELETE / words / calle
    # ----------------------------
    # Delete existing word "calle"
    response = client.delete('/words/calle')
    assert response.status_code == 204

    # ----------------
    # 8 - GET /words
    # ----------------
    # Get the list of words, "calle" should be gone!
    response = client.get('/words')
    assert response.status_code == 200
    expected = {'data': ['cosa', 'caso', 'paco', 'pepe', 'Málaga']}
    assert expected == json.loads(response.get_data(as_text=True))
