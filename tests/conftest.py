import pytest
from flask_pymongo import PyMongo

from app import app as flask_app


@pytest.fixture(scope="module")
def app():
    yield flask_app


@pytest.fixture(scope="module")
def client(app):
    with app.app_context():
        if app.config["MONGO_URI"].find("test") != -1:
            # Reset the test database when starting the tests
            mongo = PyMongo(app)
            mongo.db.words.drop()
            mongo.db.words.create_index("word", unique=True)
        else:
            raise ValueError("IT SEEMS YOU ARE NOT USING THE TEST ENVIRONMENT!!! PLEASE CHECK!!")
    return app.test_client()
